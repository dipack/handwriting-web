let sketchpad;

function fillCanvasBkg() {
    const canvas = document.getElementById('sketchpad');
    const ctx = canvas.getContext('2d');
    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}

function main() {
    console.log("Hello world!");
    sketchpad = new Sketchpad({
        element: '#sketchpad',
        width: 400,
        height: 400,
        penSize: 10
    });
    fillCanvasBkg();
}

function undo() {
    sketchpad.undo();
}

function redo() {
    sketchpad.redo();
}

function clr() {
    sketchpad.clear();
}

function recognise() {
    // const canvas = document.getElementById('sketchpad');
    const img_data = canvasToImage('white');
    const url = "http://localhost:9091/recognise";
    const json = {'img': img_data};
    $.ajax(url, {
        type: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(json),
        dataType: 'json',
        complete: (response) => {
            if (response.readyState === 4 && response.status === 200) {
                const resp = JSON.parse(response.responseText);
                console.log(resp);
            } else {
                console.error(response);
            }
        }
    });
}

/**
 * Function to set the background colour of the canvas when exporting the image,
 * using canvas.toDataURL(..)
 * https://blogs.adobe.com/digitalmedia/2011/01/setting-the-background-color-when-generating-images-from-canvas-todataurl/
 * @param backgroundColor
 * @returns {string} base64 encoded PNG image string
 */
function canvasToImage(backgroundColor) {
    const canvas = document.getElementById('sketchpad');
    const context = canvas.getContext('2d');

    //store the current globalCompositeOperation
    const compositeOperation = context.globalCompositeOperation;
    //cache height and width
    const w = canvas.width;
    const h = canvas.height;

    let data;

    if (backgroundColor) {
        //get the current ImageData for the canvas.
        data = context.getImageData(0, 0, w, h);

        //set to draw behind current content
        context.globalCompositeOperation = "destination-over";

        //set background color
        context.fillStyle = backgroundColor;

        //draw background / rect on entire canvas
        context.fillRect(0, 0, w, h);
    }

    //get the image data from the canvas
    const imageData = canvas.toDataURL("image/png");

    if (backgroundColor) {
        //clear the canvas
        context.clearRect(0, 0, w, h);

        //restore it with original / cached ImageData
        context.putImageData(data, 0, 0);

        //reset the globalCompositeOperation to what it was
        context.globalCompositeOperation = compositeOperation;
    }

    //return the Base64 encoded data url string
    return imageData;
}

window.onload = () => {
    main();
}

