## Handwriting Recognition - Web UI
Draw digits on the web UI, and have it be recognised by the server, running a model trained on the MNIST dataset.