import os
import logging
import keras
import glob
import numpy as np
from keras.models import Sequential, load_model
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras import backend as K
from PIL import Image

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', datefmt='%d/%m/%Y %I:%M:%S %p',
                    level=logging.INFO)


def load_hand_drawn():
    X = []
    y = []
    logging.info("Loading handdrawn images")
    base_path = "../handdrawn-images"
    for number in range(10):
        number_images = glob.glob("{0}/{1}-*.png".format(base_path, number), recursive=False)
        for n_img in number_images:
            img = Image.open(n_img)
            img = img.resize((28, 28))
            # We invert this because the Image library stores blacks as 0, and whites as 255,
            # and we need the opposite
            img_data = np.invert(img.convert("L"))
            X.append(img_data)
            y.append(number)
    return np.array(X), np.array(y)


def format_data(X, y=None, num_classes=10):
    # input image dimensions
    img_rows, img_cols = 28, 28

    if K.image_data_format() == 'channels_first':
        X = X.reshape(X.shape[0], 1, img_rows, img_cols)
        input_shape = (1, img_rows, img_cols)
    else:
        X = X.reshape(X.shape[0], img_rows, img_cols, 1)
        input_shape = (img_rows, img_cols, 1)

    X = X.astype("float32")
    X /= 255

    if y is not None:
        y = keras.utils.to_categorical(y, num_classes)

    return X, y, input_shape


def process_data(X_train, y_train, X_test, y_test, num_classes=10):
    logging.info("Processing data")

    X_train, y_train, input_shape = format_data(X_train, y_train, num_classes)
    X_test, y_test, input_shape = format_data(X_test, y_test, num_classes)
    return X_train, y_train, X_test, y_test, input_shape


def train_model(X_train, y_train, X_test, y_test, input_shape, num_classes):
    model_filename = "mnist_model.h5"
    if os.path.isfile(model_filename):
        logging.info("Loading existing model from {}".format(model_filename))
        model = load_model(model_filename)
    else:
        logging.info("Training new model")
        first_layer_nodes = 256
        epochs = 10
        batch_size = 100

        model = Sequential()
        model.add(Conv2D(32, kernel_size=(5, 5), strides=(1, 1), activation='relu', input_shape=input_shape))
        model.add(Conv2D(64, kernel_size=(3, 3), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Dropout(0.1))
        model.add(Flatten())
        model.add(Dense(first_layer_nodes, activation='relu'))
        model.add(Dense(num_classes, activation='softmax'))

        model.compile(loss=keras.losses.categorical_crossentropy,
                      optimizer=keras.optimizers.RMSprop(),
                      metrics=['accuracy'])

        model.fit(X_train, y_train,
                  validation_split=0.2,
                  epochs=epochs,
                  batch_size=batch_size,
                  callbacks=[])

        logging.info("Saving trained model to {}".format(model_filename))
        model.save(model_filename)
    return model


def load_training_data():
    (X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()
    return X_train, y_train, X_test, y_test


def main():
    num_classes = 10
    (X_train, y_train), (X_test, y_test) = keras.datasets.mnist.load_data()
    X_train, y_train, X_test, y_test, input_shape = process_data(X_train, y_train, X_test, y_test, num_classes)
    model = train_model(X_train, y_train, X_test, y_test, input_shape, num_classes)
    score = model.evaluate(X_test, y_test)
    logging.info("Model loss: {0}, accuracy: {1}".format(score[0], score[1]))
    X_test, y_test = load_hand_drawn()
    X_test, y_test, _ = format_data(X_test, y_test, num_classes)
    score = model.evaluate(X_test, y_test)
    logging.info("Hand drawn Model loss: {0}, accuracy: {1}".format(score[0], score[1]))
    return


if __name__ == "__main__":
    main()
