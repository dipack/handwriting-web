import io
import base64
import numpy as np
import json
from PIL import Image
from brain import train_model, format_data, process_data, load_training_data
from flask import Flask, request, Response

app = Flask(__name__)
brain = None


class Brain:
    def __init__(self):
        self.is_working = False
        self.model = None
        self.train_brain()
        return

    def train_brain(self):
        num_classes = 10
        X_train, y_train, X_test, y_test = load_training_data()
        X_train, y_train, X_test, y_test, input_shape = process_data(X_train, y_train, X_test, y_test, num_classes)
        model = train_model(X_train, y_train, X_test, y_test, input_shape, num_classes)
        # Have to call this function after loading a model from a h5 file
        # https://github.com/keras-team/keras/issues/6462#issuecomment-319232504
        model._make_predict_function()
        self.model = model
        return self

    def predict(self, x):
        return self.model.predict(x)


def convert_b64_to_image(b64_str: str):
    img_data = base64.b64decode(b64_str)
    img = Image.open(io.BytesIO(img_data))
    # Save the translated image locally to debug
    img.save("original.png")
    img = img.resize((28, 28))
    img.save("resized.png")
    img = np.invert(img.convert("L"))
    return img


def convert_image_to_legal_form(img):
    return img[np.newaxis, :, :]


@app.route("/recognise", methods=["POST"])
def recognise():
    b64_start_str = "data:image/png;base64,"
    img_str = request.get_json()['img']
    if img_str is None:
        return False
    if img_str.startswith(b64_start_str):
        img_str = img_str[len(b64_start_str):]
    img_data = convert_b64_to_image(img_str)
    legal_img_data = convert_image_to_legal_form(img_data)
    img, _, input_shape = format_data(legal_img_data)
    prediction = brain.predict(img)
    output = {"predicted_class": int(np.argmax(prediction)), "all_class": prediction.tolist()}
    return Response(json.dumps(output), content_type="application/json")


@app.after_request
def allow_cross_domain(response: Response):
    """Hook to set up response headers."""
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Headers'] = 'content-type'
    return response


if __name__ == "__main__":
    brain = Brain()
    app.run(port=9091)
